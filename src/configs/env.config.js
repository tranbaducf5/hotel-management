export default {
  API_BASE: "http://example.com",
  API_BASE_FULL: "http://example.com:4001/admin/v1",
  IS_PRODUCTION: 1,
};
