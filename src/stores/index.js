import { loadAccessToken, getAdminName } from "../utils/storage";

export const initialState = () => ({
  accessToken: loadAccessToken(),
  adminName: getAdminName(),
});
