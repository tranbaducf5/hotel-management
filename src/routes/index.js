// in src/customRoutes.js
import * as React from "react";
import { Route } from "react-router-dom";
import Foo from "./Foo";
import Bar from "./Bar";

export const customRoutes = [
  <Route exact path="/foo" component={Foo} />,
  <Route exact path="/bar" component={Bar} />,
];
