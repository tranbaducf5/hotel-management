import "./App.css";
import { Admin, Resource } from "react-admin";
import i18nProvider from "./provider/i18nProvider";
import authProvider from "./provider/auth-provider";
import dataProvider from "./provider/data-provider";
import { customRoutes } from "./routes/index";
import { initialState } from "./stores";
import { userReducer } from "./reducers";
import { userSaga } from "./sagas/userSaga";

import ListCustomer from "./components/Customer/list";
import CreateCustomer from "./components/Customer/create";
import EditCustomer from "./components/Customer/edit";

function App() {
  return (
    <Admin
      customSagas={[userSaga]}
      customReducers={{ userInfo: userReducer }}
      initialState={initialState}
      customRoutes={customRoutes}
      authProvider={authProvider}
      dataProvider={dataProvider}
      i18nProvider={i18nProvider}
    >
      <Resource
        name="foo"
        options={{ label: "Khách hàng " }}
        list={ListCustomer}
        edit={EditCustomer}
        create={CreateCustomer}
      />
    </Admin>
  );
}

export default App;
