import { initialState } from "../stores";

export const userReducer = (
  previousState = initialState,
  { type, payload }
) => {
  if (type === "LOGIN") {
    return { ...previousState, ...payload.data };
  }
  return previousState;
};
